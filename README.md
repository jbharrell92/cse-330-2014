I had my 2014 CSE 330 projects up, open to the world accidentally. 

I was informed that students were using my projects in order to get by easy in the class.
Not only am I disappointed that students had to resort to my (awful) repos in order to do work, but those students also did themselves a disservice by not doing the work. 

I state to this day that 330 got me the internship that lead to the job I have today. The hours I put into 330 taught me more about software engineering and web development than any other class. I would expect every student to take their work seriously. 

If you have any questions about web development or software engineering, feel free to reach out. I am happy to chat. My email is my bitbucket username @gmail.com


Jon Harrell
